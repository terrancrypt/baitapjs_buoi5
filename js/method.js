// Hàm lấy dữ liệu từ id của giao diện
function domId(id) {
  return document.getElementById(id);
}

// Hàm in kết quả ra giao diện
function inRaGiaoDien(id, noiDung) {
  document.getElementById(id).innerHTML = noiDung;
}

// Bài 1
// Hàm tổng 3 điểm 
function tong3Diem(d1, d2, d3) {
  var tong3Diem = 0;
  var tong3Diem = d1 + d2 + d3;
  return tong3Diem;
}
// Hàm tính điểm 
function tinhTongDiem(tong3Diem, khuVuc, doiTuong) {
  var tongDiem = 0;
  switch (khuVuc) {
    case 1:
      khuVuc = 2;
      break;
    case 2:
      khuVuc = 1;
      break;
    case 3:
      khuVuc = 0.5;
      break;
    default:
      khuVuc = 0;
  }

  switch (doiTuong) {
    case 1:
      doiTuong = 2.5;
      break;
    case 2:
      doiTuong = 1.5;
      break;
    case 3:
      doiTuong = 1;
      break;
    default:
      doiTuong = 0;
  }

  return (tongDiem = tong3Diem + khuVuc + doiTuong);
}

// Bài 2: 
function tinhTienDien(soKW){
  var soTien = 0;
  if (soKW <= 50){
    soTien = (soKW) * 500;
  }else if (soKW > 50 && soKW <= 100){
    soTien = (((soKW) - 50) * 650) + (50 * 500);
  }else if (soKW > 100  && soKW <= 200){
    soTien = (((soKW) - 100) * 850) + (50 * 650) + (50 *500);
  }else if (soKW > 200 && soKW <= 350){
    soTien = (((soKW) - 200) * 1100) + (100 * 850) + (50 * 650) + (50 * 500);
  }else{
    soTien = (((soKW) - 350) * 1300) + (150 * 1100) + (100 * 850) + (50 * 650) + (50 * 500);
  }
  return soTien;
}
