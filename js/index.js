// Bài tập 1: quản lý tuyển sinh
domId('btnTuyenSinh').onclick = function (){
    // input: diemChuan, khuVuc, doiTuong, diemMon1, diemMon2, diemMon3
    var diemChuan = Number(domId('diemChuan').value);
    var khuVuc = Number(domId('khuVuc').value);
    var doiTuong = Number(domId('doiTuong').value);
    var diemMon1 = Number(domId('diemMon1').value);
    var diemMon2 = Number(domId('diemMon2').value);
    var diemMon3 = Number(domId('diemMon3').value);

    // Xử lý
    tong3D = tinhTongDiem(tong3Diem(diemMon1,diemMon2,diemMon3), khuVuc, doiTuong);
    
    if (tong3D < diemChuan){
        inRaGiaoDien('ketQuaTuyenSinh', 'Bạn đã rớt. Tổng điểm: ' + tong3D);
    }else{
        inRaGiaoDien('ketQuaTuyenSinh', 'Bạn đã đậu. Tổng điểm: ' + tong3D);
    }
}

// Bài tập 2: tính tiền điện
domId('btnTienDien'). onclick = function (){
    // Input: tenNguoiDung, soKW;
    var tenNguoiDung = domId('tenNguoiDung').value;
    var soKW = Number(domId('soKW').value);

    // Xử lý
    soTienDien = tinhTienDien(soKW);

    // In kết quả ra giao diện;
    inRaGiaoDien('ketQuaTinhTienDien', 'Họ tên: ' + tenNguoiDung + '; Tiền điện: ' + soTienDien.toLocaleString() + 'VND');
}

